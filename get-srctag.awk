BEGIN {
	found = 0;
	if (length(patch_ext) < 1) {
		patch_ext = "xz";
	}
}
function is_debug() {
	if (length(debug) <= 0) {
		return 0;
	}
	return debug;
}
function patch_path(original_version, ext) {
	if (length(ext) <= 0) {
		ext = "xz"
	}
	# "https://cdn.kernel.org/pub/linux/kernel/v5.x/patch-5.19.14.xz"
	if (match(original_version, /^v?([0-9]+).([0-9]+).([0-9]+)$/, v)) {
		if (v[3] == "0") {
			return "";
		}
		ver = v[1] "." v[2] "." v[3];
		p = sprintf("https://cdn.kernel.org/pub/linux/kernel/v%d.x/patch-%s.%s", v[1], ver, ext);
		fname = "v" ver ".patch." ext
		return fname "::" p;
	} else {
		return "";
	}
}
function is_arch(v) {
	if (v ~ /[-\.]arch[0-9]+$/) {
		return 1;
	} else {
		return 0;
	}
}
function output_patch_path(original_version, tag) {
	# Do not output patch path for arch-tagged kernels
	if (is_arch(original_version) || is_arch(tag)) {
		return;
	}
	# If the tag is a full 3-component tag, don't output a patch
	if (tag ~ /^v?([0-9]+.){2}[0-9]+$/) {
		return;
	}
	# If the tag is an RC tag, don't output a patch
	if (tag ~ /-rc[0-9]+$/) {
		return;
	}
	# If we were able to generate a patch path, then output it
	pp = patch_path(original_version, patch_ext);
	if (length(pp) > 0) {
		printf "_patchsource=('%s')\n", pp;
	}
}
function output_srctag(v) {
	if (srctag ~ /'/) {
		printf "Invalid srctag contained single quotes: %s\n", srctag > "/dev/stderr";
		exit 1;
	}
	printf "_srctag='%s'\n", v;
}
function debug_type(ty) {
	if (is_debug() != 0) {
		print "found type: " ty > "/dev/stderr";
	}
}
# arch-tagged kernels
/\.arch[0-9]+$/ {
	found++;
	debug_type("arch");
	sub(/\.arch[0-9]+$/, "-" $NF);
	output_srctag("v" $0);
	exit 0;
}
# RC kernels
/\.rc[0-9]+$/ {
	found++;
	debug_type("rc");
	srctag = "v" gensub(/\.(rc[0-9]+)$/, "-\\1", "g", $0);
	output_srctag(srctag);
	if (is_debug() != 0) {
		print "original version = " $0 > "/dev/stderr";
		printf "srctag = %s\n", srctag > "/dev/stderr";
	}
	exit 0;
}
# Standard kernels
/\.[0-9]+$/ {
	found++;
	debug_type("standard");
	switch (NF) {
	case 3:
		srctag = sprintf("v%d.%d", $1, $2);
		break;
	case 2:
		srctag = "v" $0;
		break;
	default:
		printf "Invalid number of version components (%d) in standard kernel version: %s\n", NF, $0 > "/dev/stderr";
		exit 1;
		break;
	}
	output_srctag(srctag);
	if (is_debug() != 0) {
		print "original_version = " $0 > "/dev/stderr";
		printf "srctag = %s\n", srctag > "/dev/stderr";
		printf "patch_path = %s\n", patch_path($0, patch_ext) > "/dev/stderr";
	}
	output_patch_path($0, srctag);
	exit 0;
}
END {
	if (found <= 0) {
		print "No matching kernel versions found in input" > "/dev/stderr";
		exit 1;
	}
}
